<p  align="center">

<a  href="http://nestjs.com/"  target="blank"><img  src="https://nestjs.com/img/logo_text.svg"  width="320"  alt="Nest Logo"  /></a>

## Instalação
  Baixe o  repositório e execute
```bash
$ npm install
```

## Configuração
Crie um arquivo `.env` seguindo o modelo que consta em `.env.default` ou utilize o abaixo:
```
APP_UUID=c1689593-5e54-4865-a1bd-2f81ebba641b
APP_SALT=salty
APP_SESSION_DOMAIN=asd
APP_SESSION_SECRET=shhhhhh
APP_SESSION_TIMEOUT=3000
APP_PORT=3000
APP_HOST=localhost
```

O projeto já está configurado para trabalhar com um banco MySQL online (via d4free.net) mas caso queria alterar o servidor de banco de dados basta configurar o arquivo `ormconfig.json` na raiz. 
## Iniciando a aplicação
```bash
$ npm start
```

## Documentação
Após executar a aplicação, acesse localhost:3000/api para ver a estrutura da API em Swagger.
Os arquivos `Insomina.json` e `Insomnia-lint.json` contém a exportação de um environment do [Insomnia](https://insomnia.rest/) para testes da API
  

## Considerações

Escolhi utilizar o NestJS para desenvolver essa desafio pois era um framework que tinha conhecido a pouco e gostaria de experimentar mais, utilizei um banco MySQL pois pareceu adequado para as estruturas de dados do problema apesar de que um banco orientado a documentos também serviria aqui. O NestJS se mostrou um framework bem fácil de trabalhar e começar, sendo que muitas funcionalidade são gerenciadas pelo próprio framework (e.g. Rotas da api e Injeção de dependências) porém alguns conceitos dele acabaram virando barreiras na hora de realizar os testes com o Jest, não consegui que a classe de teste reconhece-se os repositórios mockados e com isso não consegui avançar com testes unitários e como não tive muito tempo para trabalhar nisso acabei decidindo não seguir com testes (apesar de achar um pecado bem ruim) para que pudesse ter alguma coisa para mostrar no final do desafio.
Obrigado pela oportunidade!