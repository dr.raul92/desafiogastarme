import { Controller, Inject, Get, Param, Post, Delete, Body, ParseIntPipe, UseGuards, Req } from '@nestjs/common';
import { CartoesService } from './cartoes.service';
import { Cartao } from './entity/cartao.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('cartoes')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('cartoes')
export class CartoesController {
    constructor(private readonly cartaoService: CartoesService) {}

    @Get()
    getAll() {
        return this.cartaoService.findAll();
    }

    @Get(':id')
    getById(@Param('id', new ParseIntPipe()) id: number) {
        return this.cartaoService.findOneById(id);
    }

    @Post()
    newCartao(@Req() req, @Body() cartao: Cartao) {
        return this.cartaoService.create(req.user.id, cartao);
    }

    @Delete(':id')
    deleteCartao(@Param('id', new ParseIntPipe()) id: number) {
        return this.cartaoService.delete(id);
    }

    @Post(':id/pagarFatura')
    pagarFatura(@Param('id', new ParseIntPipe()) id: number) {
        return this.cartaoService.replenishLimit(id);
    }
}
