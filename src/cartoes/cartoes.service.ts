import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, InsertResult, DeleteResult, FindConditions, FindOneOptions, FindManyOptions } from 'typeorm';
import { UserEntity } from 'src/user/entity/user.entity';
import { Cartao } from './entity/cartao.entity';

@Injectable()
export class CartoesService {

    constructor(
        @InjectRepository(Cartao)
        private readonly cartoesRepository: Repository<Cartao>,
    ) {}

    public async findAll(conditions?: FindManyOptions<Cartao>): Promise<Cartao[]> {
        return this.cartoesRepository.find(conditions);
    }

    public async findAllByUser(userEntity: UserEntity): Promise<Cartao[]> {
        return this.findAll({ where: { user: userEntity } });
    }

    public async findOne(conditions?: FindConditions<Cartao>, options?: FindOneOptions<Cartao>): Promise<Cartao> {
        return this.cartoesRepository.findOne(conditions, options);
    }

    public async findOneById(id: number, options?: FindOneOptions<Cartao>): Promise<Cartao> {
        return this.cartoesRepository.findOneOrFail(id, options);
    }

    public async create(user: UserEntity, cartao: Cartao): Promise<InsertResult> {
        const cartaoEntity: Cartao = this.cartoesRepository.create();
        Object.assign(cartaoEntity, cartao);
        cartaoEntity.user = user;
        return this.cartoesRepository.insert(cartaoEntity);
    }

    public async delete(id: number): Promise<DeleteResult> {
        return this.cartoesRepository.delete(id);
    }

    public async useLimitById(id: number, value: number): Promise<Cartao> {
        const cartao: Cartao = await this.findOneById(id);

        return await this.useLimit(cartao, value);
    }

    public async useLimit(cartao: Cartao, value: number): Promise<Cartao> {
        if (cartao.getRemainingLimit() < value) {
            throw Error('Cartão não tem limite suficiente');
        }

        cartao.usedLimit += value;

        this.cartoesRepository.save(cartao);

        return cartao;
    }

    public async replenishLimit(id: number): Promise<Cartao> {
        const cartao: Cartao = await this.findOneById(id);

        cartao.usedLimit = 0;
        cartao.nextPaymentDate.setMonth(cartao.nextPaymentDate.getMonth() + 1);

        this.cartoesRepository.save(cartao);

        return cartao;
    }
}
