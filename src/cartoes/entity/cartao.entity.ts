import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { IsString, IsInt, IsDateString, IsCreditCard } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserEntity } from '../../user/entity/user.entity';

@Entity()
export class Cartao {

    @ApiModelProperty()
    @PrimaryGeneratedColumn()
    public id: number;

    @ApiModelProperty()
    @Column({length: 16})
    @IsString()
    @IsCreditCard()
    public cardNumber: string;

    @ApiModelProperty()
    @Column('int', {default: '5000'})
    @IsInt()
    public limit: number;

    @ApiModelProperty()
    @Column('int', {default: '0'})
    @IsInt()
    public usedLimit: number;

    @ApiModelProperty({type: String})
    @Column('datetime', {default: null})
    @IsDateString()
    public issuedDate: Date;

    @ApiModelProperty({type: String})
    @Column('datetime', {default: null})
    @IsDateString()
    public nextPaymentDate: Date;

    @ManyToOne(type => UserEntity, user => user.cartoes)
    user: UserEntity;

    public getRemainingLimit(): number {
        return this.limit - this.usedLimit;
    }
}
