import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartoesService } from './cartoes.service';
import { CartoesController } from './cartoes.controller';
import { Cartao } from './entity/cartao.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Cartao])],
    providers: [CartoesService],
    controllers: [CartoesController],
    exports: [CartoesService],
})
export class CartoesModule {}
