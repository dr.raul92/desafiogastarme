import * as crypto from 'crypto';

export * from './entity/extended-entity';
export function passwordHash(password: string) {
    return crypto.createHmac('sha256', process.env.APP_SALT)
        .update(password)
        .digest('hex');
}
