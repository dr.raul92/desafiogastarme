import * as jwt from 'jsonwebtoken';
import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { JwtPayload } from 'src/auth/interfaces/jwt-payload.interface';
import { Credentials } from './dto/credentials';
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
    constructor(@Inject(forwardRef(() => UserService)) private readonly userService: UserService) {}

    async createToken(credentials: Credentials) {
        const user = await this.userService.login(credentials);
        const expiresIn = 60 * 60;
        const accessToken = jwt.sign({id: user.id}, process.env.APP_SESSION_SECRET, {
            expiresIn,
            audience: process.env.APP_SESSION_DOMAIN,
            issuer: process.env.APP_UUID,
        });
        return {
            expiresIn,
            accessToken,
        };
    }

    async verifyToken(token: string) {
        return new Promise(resolve => {
            jwt.verify(token, process.env.APP_SESSION_SECRET, decoded => resolve(decoded));
        });
    }

    async validateUser(payload: JwtPayload): Promise<any> {
        return await this.userService.findOneById(payload.id);
    }

}
