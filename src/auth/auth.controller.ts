import { Controller, Get, UseGuards, Body, Post, Headers } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'src/auth/auth.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Credentials } from './dto/credentials';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
constructor(private readonly authService: AuthService) {}

    @Get('verify')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    public async verify(@Headers('Authorization') token: string) {
        return this.authService.verifyToken(token);
    }

    @Post('token')
    public async getToken(@Body() credentials: Credentials) {
        return await this.authService.createToken(credentials);
    }
}
