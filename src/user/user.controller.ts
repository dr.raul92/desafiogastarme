import { Controller, Param, Get, Post, Put, Patch, Body, ParseIntPipe, Delete, UseGuards, Req } from '@nestjs/common';
import { ApiUseTags} from '@nestjs/swagger';
import { UserService} from './user.service';
import { UserEntity } from './entity/user.entity';
import { DeepPartial, DeleteResult } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('users')
@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @UseGuards(AuthGuard('jwt'))
    @Get('/')
    public async findAll(): Promise<UserEntity[]> {
        return this.userService.findAll();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/:id')
    public async findOne(@Param('id', new ParseIntPipe()) id: number) {
        return this.userService.findOneById(id, { relations: ['cartoes'] });
    }

    @Post('/')
    public async create(@Body() data: DeepPartial<UserEntity>): Promise<UserEntity> {
        return this.userService.create(data);
    }

    @UseGuards(AuthGuard('jwt'))
    @Put('/:id')
    public async update(@Body() data: DeepPartial<UserEntity>): Promise<UserEntity> {
        return this.userService.update(data);
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/:id')
    public async patch(@Param('id', new ParseIntPipe()) id: number, @Body() data: DeepPartial<UserEntity>): Promise<UserEntity> {
        return this.userService.patch(id, data);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:id')
    public async delete(@Param('id') id: number): Promise<DeleteResult> {
        return this.userService.delete(id);
    }
}
