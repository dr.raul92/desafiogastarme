import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { IsArray, IsEmail, IsString, MinLength, Validate } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { passwordHash } from '../../_helpers';
import { IsUserAlreadyExist } from '../user.validator';
import { Cartao } from '../../cartoes/entity/cartao.entity';
import { Compra } from '../../compra/entities/compra.entity';

@Entity()
export class UserEntity {

    @ApiModelProperty()
    @PrimaryGeneratedColumn()
    public id: string;

    @ApiModelProperty()
    @IsString()
    @Column()
    public name: string;

    @ApiModelProperty()
    @IsEmail()
    @Validate(IsUserAlreadyExist, { message: 'Usuario já existe' })
    @Column()
    public email: string;

    @ApiModelProperty()
    @Column()
    @MinLength(7)
    public password: string;

    @ApiModelProperty()
    @IsArray()
    @Column({type: 'text'})
    public roles: string;

    @OneToMany(type => Cartao, cartao => cartao.user)
    cartoes: Cartao[];

    @OneToMany(type => Compra, compra => compra.user)
    compras: Compra[];

    @BeforeInsert()
    hashPassword() {
        this.password = passwordHash(this.password);
    }
}
