import {ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {UserService} from './user.service';
import {Injectable} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';

@ValidatorConstraint({ name: 'isUserAlreadyExist', async: true })
@Injectable()
export class IsUserAlreadyExist implements ValidatorConstraintInterface {
    private userService: UserService;
    constructor(protected readonly moduleRef: ModuleRef) {}

    async validate(text: string) {
        if (!this.userService) {
            this.userService = this.moduleRef.get('userService');
          }
        const user = await this.userService.findOne({
            email: text,
        });
        return !user;
    }
}
