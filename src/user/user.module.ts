import { Module} from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserEntity } from './entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartoesModule } from 'src/cartoes/cartoes.module';
import { ComprasModule } from 'src/compra/compras.module';

@Module({
    imports: [TypeOrmModule.forFeature([UserEntity]), CartoesModule, ComprasModule],
    providers: [UserService],
    controllers: [UserController],
    exports: [UserService],
})
export class UserModule {}
