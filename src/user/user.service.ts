import { Repository, FindConditions, FindOneOptions, DeepPartial, DeleteResult } from 'typeorm';
import { Injectable, NotFoundException, Inject } from '@nestjs/common';
import { passwordHash } from '../_helpers';
import { Credentials } from '../auth/dto/credentials';
import { UserEntity } from './entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ) {}

    public async login(credentials: Credentials): Promise<UserEntity> {
        const user = await this.userRepository.findOne({
            email: credentials.email,
            password: passwordHash(credentials.password),
        });

        if (!user) {
            throw new NotFoundException('Usuario não encontrado');
        }

        return user;
    }

    public async findAll(): Promise<UserEntity[]> {
        return await this.userRepository.find();
    }

    public async findOneById(id: number, options?: FindOneOptions<UserEntity>): Promise<UserEntity> {
        return this.userRepository.findOneOrFail(id, options);
    }

    public async findOne(conditions?: FindConditions<UserEntity>, options?: FindOneOptions<UserEntity>): Promise<UserEntity> {
        return this.userRepository.findOne(conditions, options);
    }

    public async create(data: DeepPartial<UserEntity>): Promise<UserEntity> {
        const entity: UserEntity = this.userRepository.create(data);
        this.userRepository.save(entity);
        return entity;
    }

    public async update(data: DeepPartial<UserEntity>): Promise<UserEntity> {
        return this.create(data);
    }

    public async patch(id: number, data: DeepPartial<UserEntity>): Promise<UserEntity> {
        const entity: UserEntity = await this.findOneById(id);
        Object.assign(entity, data);
        return entity;
    }

    public async delete(id: number): Promise<DeleteResult> {
        return this.userRepository.delete(id);
    }
}
