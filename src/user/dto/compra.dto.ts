import { IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CompraDTO {

    @ApiModelProperty()
    @IsNumber()
    readonly valor: number;

    @ApiModelProperty()
    @IsString()
    readonly detalhe: string;
}
