import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartoesModule } from './cartoes/cartoes.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ComprasModule } from './compra/compras.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule,
    UserModule,
    CartoesModule,
    ComprasModule,
  ],
})
export class AppModule {}
