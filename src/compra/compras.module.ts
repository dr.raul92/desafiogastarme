import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Compra } from './entities/compra.entity';
import { CartoesModule } from 'src/cartoes/cartoes.module';
import { ComprasController } from './compras.controller';
import { ComprasService } from './compras.service';

@Module({
    imports: [TypeOrmModule.forFeature([Compra]), CartoesModule],
    controllers: [ComprasController],
    providers: [ComprasService],
    exports: [ComprasService],
})
export class ComprasModule {}
