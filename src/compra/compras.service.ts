import { Repository, FindOneOptions, InsertResult } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cartao } from 'src/cartoes/entity/cartao.entity';
import { CartoesService } from 'src/cartoes/cartoes.service';
import { Compra } from './entities/compra.entity';
import { UserEntity } from 'src/user/entity/user.entity';
import { CompraDTO } from 'src/user/dto/compra.dto';
import { CompraStatus } from './dto/compra-status.dto';

@Injectable()
export class ComprasService {

    constructor(
        @InjectRepository(Compra)
        private readonly comprasRepository: Repository<Compra>,
        @Inject(CartoesService)
        private readonly cartoesService: CartoesService,
    ) {}

    public async realizarCompra(userEntity: UserEntity, compra: CompraDTO): Promise<CompraStatus> {
        let cards: Cartao[] = await this.cartoesService.findAllByUser(userEntity);

        if (!cards || cards.length === 0) {
            return new CompraStatus('error', 'Usuario não tem cartões cadastrados', null);
        }

        cards = cards.filter(card => card.getRemainingLimit() >= compra.valor);

        if (cards.length === 0) {
            return new CompraStatus('error', 'Usuario não possui cartão com limite suficiente', null);
        }

        cards = cards.sort(this.sortCardsByLatestPayDateAndLowestLimit);

        this.cartoesService.useLimitById(cards[0].id, compra.valor);

        const compraEntity: Compra = await this.comprasRepository.create({
            purchaseDate: new Date().toISOString(),
            purchaseDetail: compra.detalhe,
            purchaseValue: compra.valor,
            user: userEntity,
            cardNumber: cards[0].cardNumber,
        });

        const result: InsertResult = await this.comprasRepository.insert(compraEntity);

        return new CompraStatus('ok', 'Compra realizada', compraEntity);
    }

    private sortCardsByLatestPayDateAndLowestLimit(a: Cartao, b: Cartao): number {
        if (a.nextPaymentDate > b.nextPaymentDate) {
            return 1;
        } else if (a.nextPaymentDate < b.nextPaymentDate) {
            return -1;
        }

        if (a.getRemainingLimit() > b.getRemainingLimit()) {
            return 1;
        } else if (a.getRemainingLimit() < b.getRemainingLimit()) {
            return -1;
        }

        return 0;
    }

    public async findAll(): Promise<Compra[]> {
        return this.comprasRepository.find();
    }

    public async findAllByUser(userEntity: UserEntity): Promise<Compra[]> {
        return this.comprasRepository.find({ where: { user: userEntity } });
    }

    public async findOneById(id: number, options?: FindOneOptions<Compra>): Promise<Compra> {
        return this.comprasRepository.findOneOrFail(id, options);
    }
}
