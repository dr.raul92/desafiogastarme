import { Compra } from '../entities/compra.entity';

export class CompraStatus {

    public status: string;

    public message: string;

    public compra: Compra;

    constructor(status: string, message: string, compra: Compra) {
        this.status = status;
        this.message = message;
        this.compra = compra;
    }
}
