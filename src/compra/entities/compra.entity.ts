import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { IsDateString, IsNumber, IsString, IsCreditCard } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserEntity } from '../../user/entity/user.entity';

@Entity()
export class Compra {

    @PrimaryGeneratedColumn()
    public id: number;

    @ApiModelProperty()
    @Column('datetime')
    @IsDateString()
    public purchaseDate: string;

    @ApiModelProperty()
    @Column()
    @IsNumber()
    public purchaseValue: number;

    @ApiModelProperty()
    @Column()
    @IsString()
    public purchaseDetail: string;

    @Column({length: 16})
    @IsString()
    @IsCreditCard()
    public cardNumber: string;

    @ManyToOne(type => UserEntity, user => user.compras)
    user: UserEntity;

}
