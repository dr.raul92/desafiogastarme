import { Controller, Inject, Get, Param, Post, Delete, Body, ParseIntPipe, UseGuards, Req } from '@nestjs/common';
import { CartoesService } from 'src/cartoes/cartoes.service';
import { Cartao } from 'src/cartoes/entity/cartao.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ComprasService } from './compras.service';
import { CompraDTO } from 'src/user/dto/compra.dto';

@ApiUseTags('compras')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('compras')
export class ComprasController {
    constructor(private readonly comprasService: ComprasService) {}

    @Get()
    getAllByUser(@Req() req) {
        return this.comprasService.findAllByUser(req.user);
    }

    @Get('/all')
    getAll() {
        return this.comprasService.findAll();
    }

    @Get(':id')
    getById(@Param('id', new ParseIntPipe()) id: number) {
        return this.comprasService.findOneById(id);
    }

    @Post()
    newCompra(@Req() req, @Body() compra: CompraDTO) {
        return this.comprasService.realizarCompra(req.user, compra);
    }
}
